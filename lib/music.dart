import 'package:flutter/material.dart';
import 'package:mywin/components/music_header.dart';
import 'package:mywin/play_bar.dart';
import 'package:window_manager/window_manager.dart';

import 'components/music_menu.dart';
import 'main_box.dart';

class Music extends StatefulWidget {
  const Music({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<Music> createState() => _MusicState();
}

class _MusicState extends State<Music> with WindowListener {
  @override
  void initState() {
    windowManager.addListener(this);
    super.initState();
  }

  @override
  void dispose() {
    windowManager.removeListener(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          MusicHeader(
            height: 60.0,
            buttonSize: 32,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 12.0, right: 8.0),
                  child: Image.asset(
                    "assets/ease.png",
                    width: 24,
                    height: 24,
                  ),
                ),
                Text(
                  widget.title,
                  style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 19.0,
                  ),
                ),
              ],
            ),
          ),
          // const Divider(height: 2.0, thickness: 2.0, color: Colors.red),
          const Expanded(
            child: Row(
              children: [MusicMenu(), Expanded(child: MainBox())],
            ),
          ),
          const PlayBar(),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
