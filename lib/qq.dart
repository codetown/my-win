// Don't forget to make the changes mentioned in
// https://github.com/bitsdojo/bitsdojo_window#getting-started

// QQ
import 'package:flutter/material.dart';
import 'components/right_side.dart';
import 'components/left_side.dart';
import 'components/qq_header.dart';

class QQ extends StatelessWidget {
  const QQ({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Material(
      child: Column(
        children: [
          QQHeader(
            height: 48,
            buttonSize: 24,
            child: Center(
              child: Text(
                '企鹅大侠',
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
            ),
          ),
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(width: 200, child: LeftSide()),
                Expanded(
                  // child: Column(children: [
                  //   Expanded(
                  //     child: ListView.builder(
                  //         itemCount: 5,
                  //         itemBuilder: (context, index) {
                  //           return index % 2 == 0
                  //               ? Text("左")
                  //               : Icon(Icons.wallet);
                  //         }),
                  //   ),
                  //   SizedBox(
                  //     height: 256,
                  //     child: TextField(),
                  //   )
                  // ]),
                  child: RightSide(),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
