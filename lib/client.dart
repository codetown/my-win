// Don't forget to make the changes mentioned in
// https://github.com/bitsdojo/bitsdojo_window#getting-started

// 仿微信
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

import 'model/employee.dart';
import 'model/employee_data_source.dart';

const borderColor = Color(0xFFCCDDFF);

/// An object to set the employee collection data source to the datagrid. This
/// is used to map the employee data to the datagrid widget.
class Client extends StatefulWidget {
  const Client({Key? key}) : super(key: key);

  @override
  State<Client> createState() => _ClientState();
}

class _ClientState extends State<Client> {
  List<Employee> employees = <Employee>[];
  late EmployeeDataSource employeeDataSource;

  @override
  void initState() {
    super.initState();
    employees = getEmployeeData();
    employeeDataSource = EmployeeDataSource(employeeData: employees);
  }

  List<Employee> getEmployeeData() {
    return [
      Employee(10001, 'James', 'Project Lead', 20000),
      Employee(10002, 'Kathryn', 'Manager', 30000),
      Employee(10003, 'Lara', 'Developer', 15000),
      Employee(10004, 'Michael', 'Designer', 15000),
      Employee(10005, 'Martin', 'Developer', 15000),
      Employee(10006, 'Newberry', 'Developer', 15000),
      Employee(10007, 'Balnc', 'Developer', 15000),
      Employee(10008, 'Perry', 'Developer', 15000),
      Employee(10009, 'Gable', 'Developer', 15000),
      Employee(10010, 'Grimes', 'Developer', 15000)
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        child: Row(
      children: [
        const ToolBar(),
        const LeftSide(),
        RightSide(
          child: SfDataGrid(
            source: employeeDataSource,
            columnWidthMode: ColumnWidthMode.fill,
            columns: <GridColumn>[
              GridColumn(
                  columnName: 'id',
                  label: Container(
                      padding: const EdgeInsets.only(
                          top: 4, right: 8, bottom: 4, left: 8),
                      alignment: Alignment.center,
                      child: const Text(
                        'ID',
                      ))),
              GridColumn(
                  columnName: 'name',
                  label: Container(
                      padding: const EdgeInsets.all(8.0),
                      alignment: Alignment.center,
                      child: const Text('Name'))),
              GridColumn(
                  columnName: 'designation',
                  label: Container(
                      padding: const EdgeInsets.all(8.0),
                      alignment: Alignment.center,
                      child: const Text(
                        'Designation',
                        overflow: TextOverflow.ellipsis,
                      ))),
              GridColumn(
                  columnName: 'salary',
                  label: Container(
                      padding: const EdgeInsets.all(8.0),
                      alignment: Alignment.center,
                      child: const Text('Salary'))),
            ],
          ),
        )
      ],
    ));
  }
}

class ToolBar extends StatelessWidget {
  const ToolBar({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 56,
        child: Container(
            color: const Color(0xFF2e2e2e),
            child: const Column(
              children: [Expanded(child: Column())],
            )));
  }
}

class LeftSide extends StatelessWidget {
  const LeftSide({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 256.0,
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            const SizedBox(
                width: 256,
                height: 32,
                child: Padding(
                  padding: EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // const TextField(
                      //   keyboardType: TextInputType.text,
                      // ),
                      Text("搜索框"),
                    ],
                  ),
                )),
            const Divider(
              height: 0.8,
              color: Color(0xFFE2E2E2),
            ),
            Expanded(
              child: Container(
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Color(0xFFebe9e9), Color(0xFFf0efee)],
                      stops: [0.0, 1.0]),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

const backgroundStartColor = Color(0xFF4890ff);
const backgroundEndColor = Color(0xFF22ddff);

class RightSide extends StatelessWidget {
  const RightSide({Key? key, this.child = const SizedBox.expand()})
      : super(key: key);
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        decoration: const BoxDecoration(
            border:
                Border(left: BorderSide(color: Color(0xFFE2E2E2), width: 0.8))),
        child: Column(children: [
          const SizedBox(
              height: 32,
              child: Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                          child: Text(
                        "搜索",
                        style: TextStyle(fontSize: 18.0),
                      )),
                    ],
                  ))),
          const Divider(
            height: 0.8,
            color: Color(0xFFE2E2E2),
          ),
          Expanded(
            child: Material(
              color: Colors.white,
              child: child,
            ),
          ),
        ]),
      ),
    );
  }
}
