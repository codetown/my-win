// Don't forget to make the changes mentioned in
// https://github.com/bitsdojo/bitsdojo_window#getting-started
import 'package:flutter/material.dart';
import 'package:mywin/components/ding_header.dart';

import './components/rect_icon_button.dart';
import 'components/left_side.dart';
import 'components/right_side.dart';

const sidebarColor = Colors.white;

class Ding extends StatelessWidget {
  const Ding({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: sidebarColor,
      body: Column(
        children: [
          DingHeader(
            height: 40,
            buttonSize: 24,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(4.0),
                    child: Image.network(
                      'https://gitlab.com/codetown/codedata/-/raw/master/avatars/avt107.jpg',
                      width: 24,
                      height: 24,
                    ),
                  ),
                ),
              ],
            ),
          ),
          const Expanded(
              child: Row(
            children: [
              SizedBox(width: 48, child: ToolBar()),
              SizedBox(width: 240, child: LeftSide()),
              Expanded(child: RightSide())
            ],
          )),
        ],
      ),
    );
  }
}

class ToolBar extends StatelessWidget {
  const ToolBar({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.grey[200],
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
            child: RectIconButton(
              // tooltip: '消息',
              size: 24,
              // color: Colors.grey[300],
              // hoverColor: Colors.white,
              onPressed: () {},
              icon: const Icon(
                Icons.message_outlined,
                color: Colors.grey,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: RectIconButton(
              size: 24,
              // color: Colors.grey[300],
              // hoverColor: Colors.white,
              onPressed: () {},
              icon: const Icon(
                Icons.file_present_outlined,
                color: Colors.grey,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: RectIconButton(
              size: 24,
              // color: Colors.grey[300],
              // hoverColor: Colors.white,
              onPressed: () {},
              icon: const Icon(
                Icons.person_pin_outlined,
                color: Colors.grey,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: RectIconButton(
              size: 24,
              // color: Colors.grey[300],
              // hoverColor: Colors.white,
              onPressed: () {},
              icon: const Icon(
                Icons.phone_iphone_outlined,
                color: Colors.grey,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: RectIconButton(
              size: 24,
              // color: Colors.grey[300],
              // hoverColor: Colors.white,
              onPressed: () {},
              icon: const Icon(
                Icons.category_outlined,
                color: Colors.grey,
              ),
            ),
          ),
          const Spacer(),
          Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: RectIconButton(
              size: 24,
              // color: Colors.grey[300],
              // hoverColor: Colors.white,
              onPressed: () {},
              icon: const Icon(
                Icons.star_border_outlined,
                color: Colors.grey,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 24.0),
            child: RectIconButton(
              size: 24,
              // color: Colors.grey[300],
              // hoverColor: Colors.white,
              onPressed: () {},
              icon: const Icon(
                Icons.mail_outline_outlined,
                color: Colors.grey,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
