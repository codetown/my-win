import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crypto/crypto.dart';

class Util {
  static const String baseUrl = "http://192.168.1.4:8080";
  static const TextStyle tilteStyle = TextStyle(
      fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.black);
  static const TextStyle subtilteStyle =
      TextStyle(fontSize: 14.0, color: Colors.grey);
  static void toast(String words) {
    Fluttertoast.showToast(
      msg: words,
      toastLength: Toast.LENGTH_SHORT,
      backgroundColor: const Color(0xaa000000),
      textColor: Colors.white,
      gravity: ToastGravity.CENTER,
    );
  }

  static Future<bool> setInfo(String key, Object object) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = json.encode(object);
    return prefs.setString(key, stringValue);
  }

  static Future<String?> getInfo(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  static Future<bool> removeInfo(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.remove(key);
  }

  static void push(BuildContext context, Widget page) {
    Navigator.of(context).push(
      PageRouteBuilder(
        pageBuilder: (BuildContext context, Animation<double> animation,
            Animation<double> secondaryAnimation) {
          return page;
        },
      ),
    );
  }

  static void pushReplacement(BuildContext context, Widget page) {
    Navigator.of(context).pushReplacement(
      PageRouteBuilder(
        pageBuilder: (BuildContext context, Animation<double> animation,
            Animation<double> secondaryAnimation) {
          return page;
        },
      ),
    );
  }

  static void pop(BuildContext context) {
    Navigator.of(context).pop();
  }

  static bool isPhone(String input) {
    RegExp mobile = RegExp(r"1[0-9]\d{9}$");
    return mobile.hasMatch(input);
  }

  static bool isLoginPassword(String input) {
    RegExp mobile = RegExp(r"(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$");
    return mobile.hasMatch(input);
  }

  static bool isEmail(String input) {
    RegExp email = RegExp(r"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
    return email.hasMatch(input);
  }

  static String toMD5(String text, {bool upperCase = false}) {
    var content = utf8.encode(text);
    text = md5.convert(content).toString();
    if (upperCase) {
      text = text.toUpperCase();
    }
    return text;
  }
}
