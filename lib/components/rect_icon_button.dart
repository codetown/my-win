import 'package:flutter/material.dart';

class RectIconButton extends StatelessWidget {
  final Icon icon;
  final double radius;
  final double size;
  final void Function() onPressed;
  const RectIconButton(
      {Key? key,
      required this.icon,
      this.radius = 8,
      this.size = 32,
      required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius),
      child: SizedBox(
        width: 32.0,
        height: 32.0,
        child: InkWell(
          onTap: () {
            onPressed();
          },
          child: icon,
        ),
      ),
    );
  }
}
