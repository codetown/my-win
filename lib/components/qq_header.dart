import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mywin/components/rect_icon_button.dart';
import 'package:window_manager/window_manager.dart';

const borderColor = Color(0xFFCCDDFF);

const sidebarColor = Color(0xFF20cefe);

const backgroundStartColor = Color(0xFF4890ff);
const backgroundEndColor = Color(0xFF22ddff);

class QQHeader extends StatelessWidget {
  final double height;
  final double buttonSize;
  final Widget child;

  const QQHeader(
      {Key? key,
      this.height = 32,
      this.buttonSize = 32,
      this.child = const SizedBox.expand()})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final needMoveWindow = const [
      TargetPlatform.windows,
      TargetPlatform.macOS,
    ].contains(defaultTargetPlatform);
    if (!needMoveWindow) {
      return child;
    }
    return DecoratedBox(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [backgroundStartColor, backgroundEndColor],
            stops: [0.0, 1.0]),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: 200,
            height: height - (height - buttonSize) / 2,
            child: const Padding(
              padding: EdgeInsets.only(left: 8.0, right: 8.0),
              child: CupertinoTextField(
                placeholder: '搜索',
                placeholderStyle: TextStyle(color: Colors.white54),
                padding: EdgeInsets.only(
                  left: 8,
                  top: 2.0,
                  bottom: 2.0,
                  right: 8,
                ),
                style: TextStyle(color: Colors.white, letterSpacing: 1),
                // placeholderStyle: TextStyle(fontSize: 14, height: 1.25),
                decoration: BoxDecoration(
                  // 文本框装饰
                  color: Color.fromRGBO(255, 255, 255, 0.2), // 文本框颜色
                  //border: Border.all(color: const Color(0xffdddddd), width: 1), // 输入框边框
                  borderRadius: BorderRadius.all(Radius.circular(4)), // 输入框圆角设置
                  // boxShadow: [
                  //   BoxShadow(color: Colors.redAccent, offset: Offset(0, 5))
                  // ], //装饰阴影
                ),
              ),
            ),
          ),
          RectIconButton(
            icon: const Icon(
              Icons.add,
              color: Colors.white,
            ),
            onPressed: () {},
          ),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onPanStart: (details) {
                windowManager.startDragging();
              },
              onDoubleTap: () async {
                if (await windowManager.isMaximized()) {
                  windowManager.restore();
                } else {
                  windowManager.maximize();
                }
              },
              child: SizedBox(height: height, child: child),
            ),
          ),
          if (height > buttonSize)
            SizedBox(
              height: height,
              width: (height - buttonSize) / 2,
            ),
          // 最小化按钮
          SizedBox(
            width: buttonSize,
            height: buttonSize,
            child: InkWell(
              onTap: () async {
                if (!await windowManager.isMinimized()) {
                  windowManager.minimize();
                }
              },
              child: Icon(
                Icons.minimize_outlined,
                color: Colors.white,
                size: height - 24,
              ),
            ),
          ),
          if (height > buttonSize)
            SizedBox(
              height: height,
              width: (height - buttonSize) / 2,
            ),
          // 最大化按钮
          SizedBox(
            width: buttonSize,
            height: buttonSize,
            child: InkWell(
              onTap: () async {
                if (await windowManager.isMaximized()) {
                  windowManager.unmaximize();
                } else {
                  windowManager.maximize();
                }
              },
              child: Icon(
                Icons.square_outlined,
                color: Colors.white,
                size: height - 24,
              ),
            ),
          ),
          if (height > buttonSize)
            SizedBox(
              height: height,
              width: (height - buttonSize) / 2,
            ),
          // 关闭按钮
          SizedBox(
            width: buttonSize,
            height: buttonSize,
            child: InkWell(
              onTap: () {
                windowManager.close();
              },
              hoverColor: Colors.red,
              child: Icon(
                Icons.close,
                color: Colors.white,
                size: height - 24,
              ),
            ),
          ),
          if (height > buttonSize)
            SizedBox(
              height: height,
              width: (height - buttonSize) / 2,
            ),
        ],
      ),
    );
  }
}
