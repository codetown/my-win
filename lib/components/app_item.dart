import 'package:flutter/material.dart';

class AppItem extends StatelessWidget {
  const AppItem(
      {Key? key,
      required this.width,
      required this.imgUrl,
      this.ratio = 1.0,
      required this.title,
      required this.onTap})
      : super(key: key);
  final double width;
  final String imgUrl;
  final double ratio;
  final String title;
  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          onTap: () {
            onTap();
          },
          child: Column(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(4.0),
                child: Image.network(
                  imgUrl,
                  width: width - 32.0,
                  height: width * ratio,
                  fit: BoxFit.fill,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                child: Text(
                  title,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(fontSize: 12.0, height: 1.25),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
