import 'package:flutter/material.dart';
import 'package:mywin/util.dart';

class RightSide extends StatelessWidget {
  const RightSide({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          border: Border(left: BorderSide(color: Color(0xFFEFEFEF), width: 1))),
      child: Column(children: [
        Container(
          padding: const EdgeInsets.only(
              left: 16.0, top: 12.0, bottom: 12.0, right: 16.0),
          height: 64.0,
          decoration: const BoxDecoration(
            color: Colors.white,
            border: Border(
              top: BorderSide(color: Color(0xFFEEEEEE), width: 1),
            ),
          ),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(4),
                  child: Image.network(
                      'https://gitlab.com/codetown/codedata/-/raw/master/avatars/avt108.jpg'),
                ),
              ),
              const Text(
                "孙猴子",
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w600),
              )
            ],
          ),
        ),
        Expanded(
            child: Container(
          decoration: const BoxDecoration(
            color: Color(0xFFFAFAFA),
            border: Border(
              top: BorderSide(color: Color(0xFFEEEEEE), width: 1),
              bottom: BorderSide(color: Color(0xFFEEEEEE), width: 1),
            ),
          ),
        )),
        Container(
          height: 40.0,
          padding: const EdgeInsets.only(left: 16.0, right: 16.0),
          decoration: const BoxDecoration(
            color: Colors.white,
            border: Border(
              //top: BorderSide(color: Color(0xFFEEEEEE), width: 1),
              bottom: BorderSide(color: Color(0xFFEEEEEE), width: 1),
            ),
          ),
        ),
        const SizedBox(
          height: 106.0,
          child: TextField(
            style: TextStyle(fontSize: 14.0, height: 1.5),
            keyboardType: TextInputType.multiline,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xFFF6F6F6),
                hoverColor: Color(0xFFF6F6F6),
                filled: true,
                contentPadding: EdgeInsets.only(
                    top: 8.0, right: 2.0, bottom: 8.0, left: 10.0)),
            maxLines: 10,
          ),
        ),
        Container(
          height: 44.0,
          padding: const EdgeInsets.only(left: 16.0, right: 16.0),
          decoration: const BoxDecoration(
              color: Colors.white,
              border: Border(
                top: BorderSide(color: Color(0xFFEEEEEE), width: 1),
              )),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const Padding(
                padding: EdgeInsets.only(right: 8.0),
                child: Text(
                  "Enter键发送，Ctrl+Enter键换行",
                  style: TextStyle(color: Color(0xFFCCCCCC)),
                ),
              ),
              MaterialButton(
                height: 32.0,
                highlightElevation: 0.8,
                elevation: 0.8,
                color: const Color(0xFF0088DD),
                minWidth: 72.0,
                onPressed: () {
                  Util.pop(context);
                },
                child: const Text(
                  "发送",
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
