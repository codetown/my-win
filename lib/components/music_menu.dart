import 'package:flutter/material.dart';

class MusicMenu extends StatelessWidget {
  const MusicMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List friendList = [
      {
        'title': '发现音乐',
        'subtitle': '花果山水帘洞美猴王齐天大圣',
        'imageUrl':
            'https://gitlab.com/codetown/codedata/-/raw/master/avatars/xy02.png'
      },
      {
        'title': '播客',
        'subtitle': '火焰山芭蕉洞牛魔王平天大圣',
        'imageUrl':
            'https://gitlab.com/codetown/codedata/-/raw/master/avatars/avt101.jpg'
      },
      {
        'title': '视频',
        'subtitle': '福林山云栈洞猪妖王前世天蓬元帅',
        'imageUrl':
            'https://gitlab.com/codetown/codedata/-/raw/master/avatars/xy03.png'
      },
      {
        'title': '关注',
        'subtitle': '八百里流沙河前世卷帘大将',
        'imageUrl':
            'https://gitlab.com/codetown/codedata/-/raw/master/avatars/xy05.png'
      },
      {
        'title': '直播',
        'subtitle': '鹰愁涧',
        'imageUrl':
            'https://gitlab.com/codetown/codedata/-/raw/master/avatars/xy01.png'
      },
      {
        'title': '私人FM',
        'subtitle': '西海龙宫',
        'imageUrl':
            'https://gitlab.com/codetown/codedata/-/raw/master/avatars/xy04.png'
      },
      {
        'title': '我的音乐',
        'subtitle': '不知道从哪找来的',
        'imageUrl':
            'https://gitlab.com/codetown/codedata/-/raw/master/avatars/avt102.jpg'
      },
      {
        'title': '创作的歌单',
        'subtitle': '房产销售',
        'imageUrl':
            'https://gitlab.com/codetown/codedata/-/raw/master/avatars/avt106.jpg'
      },
      {
        'title': '我喜欢的音乐',
        'subtitle': '买海鲜的',
        'imageUrl':
            'https://gitlab.com/codetown/codedata/-/raw/master/avatars/avt103.jpg'
      },
    ];
    return Container(
      width: 200,
      padding: const EdgeInsets.all(16.0),
      decoration: const BoxDecoration(
        color: Colors.white,
        border: Border(
          right: BorderSide(
            width: 1,
            color: Color(0xFFEEEEEE),
          ),
        ),
      ),
      child: ListView.separated(
        separatorBuilder: (_, index) => const Divider(
          height: 2,
          color: Colors.white,
        ),
        itemCount: friendList.length,
        itemBuilder: (_, index) => ClipRRect(
          borderRadius: BorderRadius.circular(4.0),
          child: Material(
            color: Colors.white,
            child: InkWell(
              onTap: () {},
              hoverColor: const Color(0xFFEEEEEE),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  friendList[index]['title'],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
